EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 9
Title "PQ9ISH Sensor Module"
Date "2018-04-30"
Rev ""
Comp "Libre Space Foundation"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5700 3950 0    50   Input ~ 0
MQ-7_VRL
Text HLabel 4250 4000 0    50   Input ~ 0
S_Power
$Comp
L lsf-kicad:Si1401EDH Q3
U 1 1 5AE99F31
P 4800 3600
AR Path="/5AE9F968/5AE99F31" Ref="Q3"  Part="1" 
AR Path="/5AE772E0/5AE99F31" Ref="Q1"  Part="1" 
AR Path="/5AE77977/5AE99F31" Ref="Q2"  Part="1" 
AR Path="/5AEA0DE2/5AE99F31" Ref="Q4"  Part="1" 
AR Path="/5AE99F31" Ref="Q1"  Part="1" 
F 0 "Q1" V 5425 3600 50  0000 C CNN
F 1 "SSM6J216FE" V 5334 3600 50  0000 C CNN
F 2 "lsf-kicad-lib:SOT-563" H 5000 3700 50  0001 C CNN
F 3 "http://www.vishay.com/docs/70080/si1401ed.pdf" H 4800 3600 50  0001 C CNN
F 4 "SSM6J216FE" H 0   -100 50  0001 C CNN "Part Number"
	1    4800 3600
	0    1    -1   0   
$EndComp
$Comp
L Device:R R7
U 1 1 5AE9A012
P 4450 3850
AR Path="/5AE9F968/5AE9A012" Ref="R7"  Part="1" 
AR Path="/5AE772E0/5AE9A012" Ref="R5"  Part="1" 
AR Path="/5AE77977/5AE9A012" Ref="R6"  Part="1" 
AR Path="/5AEA0DE2/5AE9A012" Ref="R8"  Part="1" 
F 0 "R5" H 4520 3896 50  0000 L CNN
F 1 "100K" H 4520 3805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.99x1.00mm_HandSolder" V 4380 3850 50  0001 C CNN
F 3 "~" H 4450 3850 50  0001 C CNN
F 4 "CRCW0603100KFKEA" H 0   0   50  0001 C CNN "Part Number"
	1    4450 3850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4250 4000 4450 4000
Wire Wire Line
	4800 4000 4800 3900
Wire Wire Line
	4450 4000 4800 4000
Wire Wire Line
	4450 3700 4450 3600
Connection ~ 4450 3600
Wire Wire Line
	4450 3600 4600 3600
Connection ~ 5000 3600
Connection ~ 4450 4000
$Comp
L Device:C C3
U 1 1 5AEBDF82
P 5000 4200
AR Path="/5AE772E0/5AEBDF82" Ref="C3"  Part="1" 
AR Path="/5AE77977/5AEBDF82" Ref="C4"  Part="1" 
AR Path="/5AE9F968/5AEBDF82" Ref="C7"  Part="1" 
AR Path="/5AEA0DE2/5AEBDF82" Ref="C8"  Part="1" 
F 0 "C3" H 5115 4246 50  0000 L CNN
F 1 "47uF" H 5115 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.39x1.80mm_HandSolder" H 5038 4050 50  0001 C CNN
F 3 "~" H 5000 4200 50  0001 C CNN
F 4 "GRM31CE70J476ME15L" H 5000 4200 50  0001 C CNN "Part Number"
	1    5000 4200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5000 4050 5000 3600
$Comp
L power:GND #PWR023
U 1 1 5AEBE1ED
P 5000 4400
AR Path="/5AE772E0/5AEBE1ED" Ref="#PWR023"  Part="1" 
AR Path="/5AE77977/5AEBE1ED" Ref="#PWR024"  Part="1" 
AR Path="/5AE9F968/5AEBE1ED" Ref="#PWR025"  Part="1" 
AR Path="/5AEA0DE2/5AEBE1ED" Ref="#PWR026"  Part="1" 
F 0 "#PWR023" H 5000 4150 50  0001 C CNN
F 1 "GND" H 5005 4227 50  0000 C CNN
F 2 "" H 5000 4400 50  0001 C CNN
F 3 "" H 5000 4400 50  0001 C CNN
	1    5000 4400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5000 4400 5000 4350
$Comp
L lsf-kicad:MQ-7 S1
U 1 1 5AEE1A1B
P 6250 3950
AR Path="/5AEE1A1B" Ref="S1"  Part="1" 
AR Path="/5AE772E0/5AEE1A1B" Ref="S1"  Part="1" 
F 0 "S1" H 6500 4250 50  0000 L CNN
F 1 "MQ-7" H 6500 4150 50  0000 L CNN
F 2 "lsf-kicad-lib:MQ-7A" H 6200 4450 50  0001 C CNN
F 3 "https://www.sparkfun.com/datasheets/Sensors/Biometric/MQ-7.pdf" H 6550 4550 50  0001 C CNN
	1    6250 3950
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5AEE1A2F
P 6250 4400
F 0 "#PWR08" H 6250 4150 50  0001 C CNN
F 1 "GND" H 6255 4227 50  0000 C CNN
F 2 "" H 6250 4400 50  0001 C CNN
F 3 "" H 6250 4400 50  0001 C CNN
	1    6250 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5AEE1A35
P 5800 4200
F 0 "R6" H 5870 4246 50  0000 L CNN
F 1 "3.6K" H 5870 4155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.99x1.00mm_HandSolder" V 5730 4200 50  0001 C CNN
F 3 "~" H 5800 4200 50  0001 C CNN
F 4 "RR0816P-362-D" H 0   0   50  0001 C CNN "Part Number"
	1    5800 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5AEE1A3C
P 5800 4400
F 0 "#PWR07" H 5800 4150 50  0001 C CNN
F 1 "GND" H 5805 4227 50  0000 C CNN
F 2 "" H 5800 4400 50  0001 C CNN
F 3 "" H 5800 4400 50  0001 C CNN
	1    5800 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3950 5800 4050
Wire Wire Line
	5800 4350 5800 4400
Wire Wire Line
	6250 4300 6250 4400
Text Label 5400 3600 0    50   ~ 0
MQ-7_VH
Text GLabel 4250 3600 0    50   Input ~ 0
5V
Wire Wire Line
	4450 3600 4250 3600
Wire Wire Line
	5700 3950 5800 3950
Wire Wire Line
	5000 3600 6250 3600
Text GLabel 6850 3950 2    50   Input ~ 0
5V
Wire Wire Line
	6850 3950 6700 3950
Wire Wire Line
	5000 3300 5000 3400
Wire Wire Line
	5000 3600 5000 3500
Connection ~ 5000 3400
Connection ~ 5000 3500
Wire Wire Line
	5000 3500 5000 3400
$EndSCHEMATC
