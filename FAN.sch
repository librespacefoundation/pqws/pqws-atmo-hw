EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5400 3150 0    50   Input ~ 0
FAN_Pulse
Text HLabel 3400 3650 0    50   Input ~ 0
S_Power
$Comp
L Device:R R901
U 1 1 5AF0604C
P 3600 3500
F 0 "R901" H 3670 3546 50  0000 L CNN
F 1 "100K" H 3670 3455 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.99x1.00mm_HandSolder" V 3530 3500 50  0001 C CNN
F 3 "~" H 3600 3500 50  0001 C CNN
F 4 "CRCW0603100KFKEA" H 0   0   50  0001 C CNN "Part Number"
	1    3600 3500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3400 3650 3600 3650
Wire Wire Line
	3950 3650 3950 3550
Wire Wire Line
	3600 3650 3950 3650
Wire Wire Line
	3600 3350 3600 3250
Connection ~ 3600 3250
Wire Wire Line
	3600 3250 3750 3250
Connection ~ 4150 3250
Connection ~ 3600 3650
$Comp
L Device:C C901
U 1 1 5AF06061
P 4150 3850
F 0 "C901" H 4265 3896 50  0000 L CNN
F 1 "47uF" H 4265 3805 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.39x1.80mm_HandSolder" H 4188 3700 50  0001 C CNN
F 3 "~" H 4150 3850 50  0001 C CNN
F 4 "GRM31CE70J476ME15L" H 4150 3850 50  0001 C CNN "Part Number"
	1    4150 3850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4150 3700 4150 3250
$Comp
L power:GND #PWR0901
U 1 1 5AF06069
P 4150 4050
F 0 "#PWR0901" H 4150 3800 50  0001 C CNN
F 1 "GND" H 4155 3877 50  0000 C CNN
F 2 "" H 4150 4050 50  0001 C CNN
F 3 "" H 4150 4050 50  0001 C CNN
	1    4150 4050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4150 4050 4150 4000
Text GLabel 2900 3250 0    50   Input ~ 0
5V
Wire Wire Line
	3600 3250 3400 3250
Wire Wire Line
	4150 3250 5400 3250
$Comp
L Connector_Generic:Conn_01x03 J901
U 1 1 5AF0699F
P 5600 3250
F 0 "J901" H 5519 2925 50  0000 C CNN
F 1 "FAN" H 5519 3016 50  0000 C CNN
F 2 "Connector_JST:JST_SH_SM03B-SRSS-TB_1x03-1MP_P1.00mm_Horizontal" H 5600 3250 50  0001 C CNN
F 3 "~" H 5600 3250 50  0001 C CNN
F 4 "SM03B-SRSS-TB" H 0   0   50  0001 C CNN "Part Number"
	1    5600 3250
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR0902
U 1 1 5AF06A48
P 5300 3450
F 0 "#PWR0902" H 5300 3200 50  0001 C CNN
F 1 "GND" H 5305 3277 50  0000 C CNN
F 2 "" H 5300 3450 50  0001 C CNN
F 3 "" H 5300 3450 50  0001 C CNN
	1    5300 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 3350 5300 3350
Wire Wire Line
	5300 3350 5300 3450
$Comp
L Device:Ferrite_Bead L901
U 1 1 5AF08530
P 3250 3250
F 0 "L901" V 2976 3250 50  0000 C CNN
F 1 "Ferrite_Bead" V 3067 3250 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad0.99x1.00mm_HandSolder" V 3180 3250 50  0001 C CNN
F 3 "~" H 3250 3250 50  0001 C CNN
F 4 "MPZ1608S101A" H 0   0   50  0001 C CNN "Part Number"
	1    3250 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	2900 3250 3100 3250
Text Label 3600 3250 0    50   ~ 0
FAN_S
Text Label 4450 3250 0    50   ~ 0
FAN_D
Wire Wire Line
	4150 2950 4150 3050
$Comp
L lsf-kicad:Si1401EDH Q901
U 1 1 5AF06045
P 3950 3250
AR Path="/5AF06045" Ref="Q901"  Part="1" 
AR Path="/5AF059E4/5AF06045" Ref="Q901"  Part="1" 
F 0 "Q901" V 4575 3250 50  0000 C CNN
F 1 "SSM6J216FE" V 4484 3250 50  0000 C CNN
F 2 "lsf-kicad-lib:SOT-563" H 4150 3350 50  0001 C CNN
F 3 "http://www.vishay.com/docs/70080/si1401ed.pdf" H 3950 3250 50  0001 C CNN
F 4 "SSM6J216FE" H 0   -100 50  0001 C CNN "Part Number"
	1    3950 3250
	0    1    -1   0   
$EndComp
Wire Wire Line
	4150 3250 4150 3150
Connection ~ 4150 3050
Connection ~ 4150 3150
Wire Wire Line
	4150 3150 4150 3050
$EndSCHEMATC
